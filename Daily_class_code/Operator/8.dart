// Operator

main(){
	int x=5;
	int y=6;
//	int ans=(++x < ++y) && (--x > ++y);  // ERROR: A value of type 'bool' can't be assigned to a variable of type 'int'.

	print(x); //5
	print(y); //6

}
