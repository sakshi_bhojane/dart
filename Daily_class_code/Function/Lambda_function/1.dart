// Lambda function

int add(int a,int b){
	return a+b;
}
int sub(int a,int b){
	return a-b;
}
int mul(int a,int b){
	return a*b;
}
int div(int a,int b){
	return a/b;  //  A value of type 'double' can't be returned from a function with return type 'int'.
}
main(){
	int a=10;
	int b=20;
	print(add(a,b));
	print(sub(a,b));
	print(mul(a,b));
	print(div(a,b));
}

