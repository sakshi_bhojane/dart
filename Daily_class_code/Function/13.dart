 // Required

void playerInfo(String team,{required int jerNo,required String name}){
	print(team);
	print(jerNo);
	print(name);
}

main(){
	playerInfo("India",jerNo:18,name:"Virat"); /*    India
							18
							Virat
						*/


	playerInfo("India",jerNo:null,name:null);   // ERROR


}

/*

 The value 'null' can't be assigned to the parameter type 'String' because 'String' is not nullable.
       
*/
