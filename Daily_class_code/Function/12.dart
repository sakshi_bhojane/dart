// Named Argument

void playerInfo(String team,{int? jerNo,String? name}){

	print(team);
	print(jerNo);
	print(name);
}

main(){
	playerInfo("India");
	playerInfo("India",jerNo:18);
	playerInfo("India",jerNo:18,name:"Virat");


}

/*

India
null
null
India
18
null
India
18
Virat

*/
