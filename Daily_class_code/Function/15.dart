// Function (Return value)

int add(int a,int b){  //A non-null value must be returned since the return type 'int' doesn't allow null.
	print(a+b);
}
void main(){
	int x=10;
	int y=20;
	int retVal=add(x,y); 
	print(retVal);
}

