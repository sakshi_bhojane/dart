// var

// when we declare var in it store integer data then we only assign integer in it...
// we cant store string or doble in it...

main(){
	var x="Shashi";
	print(x);
	x=20.5;  // Error: A value of type 'double' can't be assigned to a variable of type 'String'.
	print(x);
	x=true; // Error: A value of type 'bool' can't be assigned to a variable of type 'String'.
	print(x);
}
