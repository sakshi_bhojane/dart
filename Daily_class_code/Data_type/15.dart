// const variable

// it must be initialize as declaration time..
// as well as we cant change the value of const and cant assign

main(){

	const int x=10;

	const int z;  // The const variable 'z' must be initialized.Try adding an initializer ('= expression') to the declaration.

	x=30;  //Can't assign to the const variable 'x'.
}
