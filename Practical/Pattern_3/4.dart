/*

1 
2  4
3  6  9
4  8  12  16
5  10 15  20  25


*/

import 'dart:io';
void main(){
	
	print("Enter rows ");
	int row = int.parse(stdin.readLineSync()!);
	print("---------------");
	for(int i=1;i<=row;i++){
		int num=i;
		for(int j = 1;j<=i;j++){
			int x=num*j;
			stdout.write("$x  ");

		}
		stdout.writeln();
	}
	
}



