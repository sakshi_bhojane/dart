/*

1 
3  5  
5  8  11
7  11 15  19


*/

import 'dart:io';
void main(){
	
	print("Enter rows ");
	int row = int.parse(stdin.readLineSync()!);
	print("---------------");
	int num=1;
	for(int i=1;i<=row;i++){
		int x=num;
		for(int j = 1;j<=i;j++){
			stdout.write("$x  ");
			x=x+i;

		}
		num=num+2;
		stdout.writeln();
	}
	
}



