/*

1
3  5
7  9  11
13 15 17 19

*/

import 'dart:io';
void main(){
	
	print("Enter rows ");
	int row = int.parse(stdin.readLineSync()!);
	print("---------------");
	int num=1;
	for(int i=1;i<=row;i++){
		for(int j =1;j<=i;j++){
			if(num%2!=0){
				stdout.write("$num  ");
			}
			num=num+2;
		}
		stdout.writeln();
	}
	
}



