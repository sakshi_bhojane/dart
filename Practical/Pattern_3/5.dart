/*

10
9  8
7  6  5
4  3  2  1

*/

import 'dart:io';
void main(){
	
	print("Enter rows ");
	int row = int.parse(stdin.readLineSync()!);
	print("---------------");
	int num=10;
	for(int i=1;i<=row;i++){
		for(int j = 1;j<=i;j++){
			stdout.write("$num  ");
			num--;

		}
		stdout.writeln();
	}
	
}



