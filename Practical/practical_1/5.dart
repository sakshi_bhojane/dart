// WAP to print the square of even digits between 40 to 50

main(){

	int i=40;
	while(i<51){
		if(i%2==0){
			print(i*i);
		}
		i++;
	}
}
