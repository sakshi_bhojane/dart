class Demo1 {
  int x;
  Demo1(this.x);
}

class Demo2 extends Demo1 {
  Demo2(super.x);
  void fun() {
    print(x);
  }
}

main() {
  Demo1 obj2 = Demo2(10);
  obj2.fun();
}
/* Error:The method 'fun' isn't defined for the class 'Demo1'.*/
/* At compile time compiler check reference of Demo1 and check fun method in 
Demo1 class but fun not available in Demo1. */