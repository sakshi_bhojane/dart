abstract class Demo1 {
  factory Demo1() {
    return Demo2();
  }
}
class Demo2 implements Demo1{
  Demo2(){
    print("Demo2");
  }
}
main(){
  Demo1 obj=new Demo1();
}

/* Output: Demo2 */

/* When we create factory constructor of abstract class that time compiler 
not consider this class as abstract.Therefor here Demo1() object are created. 
And after that call Demo2 constructor and print. */