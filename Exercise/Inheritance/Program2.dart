class Test {
  int x = 30;
}

class Test2 extends Test {
  int x;
  Test2(this.x);
  void gun() {
    this.x = 8;
  }

  void fun() {
    print(this.x);
    print(super.x);
  }
}

main() {
  Test2 obj = Test2(10);
  obj.gun();
  obj.fun();
}

/* Output
8
30
*/
/* First print output as 8 because in method gun we initialiaze x as 8. 
but in second output we access the x of parent class . In which x has value 30. 
*/