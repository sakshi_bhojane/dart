class Parent {
  Parent() {
    print("In parent Constructor");
  }
}

class Child extends Parent {
  Child() {
    super();
    print("In child constructor");
  }
}

main() {
  Child obj = new Child();
}

/* Error: Superclass has no method named 'call'.*/

/* In dart when we call super method that time parent class must have call method. */