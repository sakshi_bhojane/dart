class Test {
  int x = 10;
  Test(this.x);
}

class Test2 extends Test {
  Test2(super.x);
}

void main() {
  Test2 obj = Test2(10);
  Test obj2 = Test(30);
  obj.x = 19;
  print(obj.x);
  print(obj2.x);
}

/* Output 
19
30

*/

/*  Firstly we pass 10 to test2 constructor but before print the value we put 19 in it. 
In obj2 we are passing 30 . 
*/