class Test {
  int x = 20;
  String str = "Core2web";

  void parentMethod() {
    print(x);
    print(str);
  }
}

class Test2 extends Test {
  int x = 10;
  String str = "Incubator";
  void childMethod() {
    print(x);
    print(str);
  }
}

main() {
  Test2 obj = new Test2();
  obj.parentMethod();
  obj.childMethod();
}
/* Output: 
10
Incubator
10
Incubator 
*/
/* In dart suppose we extends one class into another class. 
And same variable initialize in both class. 
And we create object of Test2 class and access the method of parent class 
as well as child class .That time only child class variable are accessed. 
*/
