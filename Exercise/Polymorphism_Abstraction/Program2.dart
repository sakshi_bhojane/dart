class Demo {
  void fun();
}

class Demo1 implements Demo {
  void fun() {}
}

main() {
  Demo obj = Demo1();
}

/* Error: The non-abstract class 'Demo' is missing implementations for these members:
 - Demo.fun 
 */
