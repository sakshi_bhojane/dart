abstract class Parent {
  int x;
  int y = 7;
  Parent(this.x);
  void printData() {
    print(x);
  }
}

class Child extends Parent {
  Child(int x, inty) : super(y); // Error
  int printData() {
    print(x);
    print(y);
    return 3;
  }
}

main() {
  Child obj = new Child(6, 7);
  obj.printData();
}

/* Error: Undefined name 'y'.
  Child(int x, inty) : super(y); */