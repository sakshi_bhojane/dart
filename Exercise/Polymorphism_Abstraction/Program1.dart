class Company {
  void companyName() {
    print("Google");
  }
}

class Employee extends Company {
  void companyName() {
    print("Apple");
  }
}

main() {
  Company obj = Employee();
  obj.companyName();
}

// Output:
// Apple