class Test {
  void fun() {
    print("Test class");
  }

  static void gun() {
    print("In static test");
  }
}

class Test2 extends Test {
  @override
  void gun() {
    print("In test2 gun");
    super.gun();
  }
}

main() {
  Test2 obj = Test2();
  obj.fun();
}

//  Error: Superclass has no method named 'gun'.
/* Static method cant be override */