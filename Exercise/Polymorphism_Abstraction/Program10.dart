abstract class Test {
  int x;
  Test(this.x) {
    print("In constructor");
  }
  void fun() {
    print("fun");
  }

  void gun();
}

class Test2 extends Test {
  Test2(super.x);
  void gun() {
    print("In gun");
  }
}

main() {
  Test2 obj = Test2(10);
  obj.fun();
  obj.gun();
}
/* Output:
In constructor
fun
In gun
*/