// Program 1

mixin class Parent {
  int x = 10;
  static int y = 199;
  void printData() {
    print(x);
    print(y);
  }
}

class Child with Parent {
  static int y = 200;
  int x = 38;
  void printData() {
    print(super.x);
    super.printData();
  }
}

main() {
  Child obj = new Child();
  obj.printData();
}
/* Output:
10
38
199
*/
/* When we call printData method that time  first line print the output 10. 
because super.x access the data of x of parent class which is 10. 
Then second line super.printData() call the method of parent class 