// Program 7

class Demo2 {
  Demo2() {
    print("In demo2");
  }
  void fun() {
    print("In fun demo2");
  }
}

mixin Demo on Demo2 {
  void fun() {
    print("In fun Demo");
  }
}

class Test extends Demo2 with Demo {
  Test() {
    print("In test");
  }
}

main() {
  Test obj = new Test();
  obj.fun();
}
/* Output: 
In demo2
In test
In fun Demo
*/