//  Program 4

mixin Test {
  int x = 20;
  void demo() {
    print("In test demo");
  }

  void fun2();
}

class Test2 with Test {
  void demo() {
    print("id test2 demo");
    super.demo();
  }

  void fun2() {
    print("IN fun2");
  }
}

main() {
  Test2 obj = new Test2();
  obj.demo();
}

/* Output:
id test2 demo
In test demo
*/