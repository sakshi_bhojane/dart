// Program 9

mixin Demo2 {
  void fun2() {
    print("In demo2 fun");
  }
}
mixin Demo on Demo2 {
  void gun() {
    print("In demo gun");
  }
}

class Test3 with Demo2 {}

main() {
  Test3 obj = new Test3();
  obj.fun2();
  obj.gun();
}
/* Error: The method 'gun' isn't defined for the class 'Test3'.*/