import 'dart:js_util';

class Employee {
  int? empId;
  String? empName;
  Employee() {}
  Employee(int empId, String empName) {} // Error
}

void main() {
  Employee obj = new Employee();
}

/*
Explination:
          Here we cant declare two constructor as same name.
          Instead of it we used named constructor in dart.
*/