class Point {
  int x;
  int y;
}

void main() {
  Point obj = Point();
}
/* Error:
        Field 'x' should be initialized because its type 'int' doesn't allow null.
  */

  /* In our program we write variable which cant store null value
  that time we are restrict to assign the value to variable.
  But we use "?" that time not necessary to assign value.
  */