class Player {
  int? jerNo;
  String? pName;

  const Player(this.jerNo, this.pName);
}

void main() {
  Player obj = const (45, "Rohit");
}


/*
  In constant constructor only final variable are allowed. 
  Here we not declared variable as final. 
  As well as here we not create object using constructor name. 
  This an another error. 
  */