class Test {
  Test._private() {
    print("In demo");
  }
  factory Test() {
    print("In demo factory");
    return Test._private();
  }
}

void main() {
  Test obj = new Test();
}

/* Output: 
In demo factory
In demo
*/
/* Here we create two constructor of class but both are having different name.*/
