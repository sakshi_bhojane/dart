class Company {
  int? empCount;
  String compName;
  Company({this.empCount, this.compName = "Deloitte"});

  void compInfo() {
    print(empCount);
    print(compName);
  }
}

void main() {
  Company obj1 = new Company(empCount:100,compName:"Veritas");
  Company obj2 = new Company(compName:"Pubmatic",empCount: 200);

  obj1.compInfo();
  obj2.compInfo();
}

/* Output:
100
Veritas
200
Pubmatic
*/

/* Here we used named constructor .
In named constructor we are passing argument as (empCount:100).
Using named constructor we are not restrication to passing argument in sequence.
*/
