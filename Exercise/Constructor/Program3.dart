class Demo {
  final int? x;
  final String? str;
  const Demo(this.x, this.str);
}

void main() {
  Demo obj1 = const Demo(10, "Core2Web");
  print(obj1.hashCode);// 226751044

  Demo obj2 = const Demo(10, "Biencaps");
  print(obj2.hashCode); // 1006528648
}

/* Here we used constant constructor . But here parameter of constructor are different
therefor hashcode of object are different. When we passing same parameter to constructor 
that time hashcode of object are same.
*/