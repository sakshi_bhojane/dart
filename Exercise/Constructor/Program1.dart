class Test{
    final int x;
    final int y;
    const Test(this.x,this.y){ // const constructor cant have body
        print("In const constructor");
    }
}
void main(){
    Test obj=Test(10,20);
    print(obj.x);
}
/*Constant constructor cant have body in dart. 
As well as we only used final variable in const constructor. */