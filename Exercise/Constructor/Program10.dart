class Demo {
  Demo() {
    print("In demo");
  }
  factory Demo() {
    print("In demo factory");
    return Demo();
  }
}

void main() {
  Demo obj = new Demo();
}

/* Error: 'Demo' is already declared in this scope. 

    In dart we cant use same name to a Constructor. 
    Another option is create multiple constructor in dart is
    a named constructor. 
    */
