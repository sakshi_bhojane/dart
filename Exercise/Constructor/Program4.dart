class Company {
  int empCount;
  String compName;
  Company(this.empCount, [this.compName = "Biencaps"]);

  void compInfo() {
    print(empCount);
    print(compName);
  }
}

void main() {
  Company obj1 = new Company(100);
  Company obj2 = new Company(200, "Pubmatic");

  obj1.compInfo();
  obj2.compInfo();
}

/* Output
100
Biencaps
200
Pubmatic
*/

/* Explination:
            In default constructor ,we have two parameter in which one is default
            it means suppose we are passing one argument that time this default parameter
            are consider.
            Her in first scenario we are passing only one argument that time
            default parameter "Biencaps" are consider. And in second scenario
            we are passing two arguments that time default parametr are not consider.
            */