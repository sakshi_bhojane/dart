void main(){
	int x=1;
	switch(x){
		case 2:
			print("Case 2");
		default:
			print("Default case");
		case 1:   // Error: The default case should be the last case in a switch statement.Try moving the default case after the other case clauses.
			print("Case 1");
	}
}
